using System;
using System.Collections.Generic;

namespace CoffeeMachine
{ 
    public class CoffeeMachine
    {
        public Dictionary<string, IRecipe> Makers { get; set; } = new Dictionary<string, IRecipe>();

        public string ProcessRequest(string request)
        {
            if (Makers.ContainsKey(request))
                return Makers[request].Make().ComposeResponse();
            
            throw new ArgumentException("Wrong Request");
        }
    }
}