﻿namespace CoffeeMachine
{
    public class LatteMaker: CoffeeMakerBase
    {
        public override Cup Make()
        {
            var cup = TakeNewEmptyCup();
            cup.Add("espresso");
            cup.Add("milk");
            cup.Add("milk");
            cup.Add("foam");
            return cup;
        }
    }
}
