﻿using System.Collections.Generic;

namespace CoffeeMachine
{
    public class CupFactory : ICupFactory
    {
        public Cup GetEmptyCup()
        {
            return new Cup(new List<string>
            {
                "espresso",
                "water",
                "milk",
                "foam"
            });
        }
    }
}
