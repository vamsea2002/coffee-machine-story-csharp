﻿namespace CoffeeMachine
{
    public interface ICupFactory
    {
        Cup GetEmptyCup();
    }
}
