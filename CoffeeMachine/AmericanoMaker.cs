﻿namespace CoffeeMachine
{
    public class AmericanoMaker: CoffeeMakerBase
    {
        public override Cup Make()
        {
            var cup = TakeNewEmptyCup();
            cup.Add("espresso");
            cup.Add("water");
            return cup;
        }
    }
}
