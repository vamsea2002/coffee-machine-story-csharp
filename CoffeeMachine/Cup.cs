using System.Collections.Generic;
using System.Linq;

namespace CoffeeMachine
{
    public class Cup
    {
        private Dictionary<string, int> ingredients = new Dictionary<string, int>();

        public Cup(List<string> zeroIngredients)
        {
            AddZeroKey(zeroIngredients);
        }

        private void AddZeroKey(List<string> keys)
        {
            keys.ForEach(key => ingredients.Add(key, 0));
        }

        public void Add(string ingredient)
        {
            if(ingredients.ContainsKey(ingredient))
                ingredients[ingredient]++;
            else
                ingredients.Add(ingredient, 1);
        }

        public string ComposeResponse()
        {
            return "{ " 
                   + string.Join(", ", ingredients.Select(x => $"\"{x.Key}\": {x.Value}").ToArray())
                   + " }";
        }
    }
}