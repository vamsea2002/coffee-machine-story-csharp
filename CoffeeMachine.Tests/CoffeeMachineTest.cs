using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoffeeMachine.Tests
{
    [TestClass]
    public class CoffeeMachineTest
    {
        private CoffeeMachine coffeeMachine = new CoffeeMachine();

        [TestInitialize]
        public void Setup()
        {
            coffeeMachine.Makers.Add("AMERICANO", new AmericanoMaker());
            coffeeMachine.Makers.Add("ESPRESSO", new EspressoMaker());
            coffeeMachine.Makers.Add("LATTE", new LatteMaker());
            coffeeMachine.Makers.Add("CAPPUCCINO", new CappuccinoMaker());
            coffeeMachine.Makers.Add("MOCACCINO", new MocaccinoMaker());
        }

        [TestMethod]
        public void ShouldGetAmericanoRecipe()
        {
            var response = coffeeMachine.ProcessRequest("AMERICANO");
            Assert.AreEqual("{ \"espresso\": 1, \"water\": 1, \"milk\": 0, \"foam\": 0 }", response);
        }

        [TestMethod]
        public void ShouldGetEspressoRecipe()
        {
            var response = coffeeMachine.ProcessRequest("ESPRESSO");
            Assert.AreEqual("{ \"espresso\": 1, \"water\": 0, \"milk\": 0, \"foam\": 0 }", response);
        }

        [TestMethod]
        public void ShouldGetLatteRecipe()
        {
            var response = coffeeMachine.ProcessRequest("LATTE");
            Assert.AreEqual("{ \"espresso\": 1, \"water\": 0, \"milk\": 2, \"foam\": 1 }", response);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ShouldThrowExceptionWhenRecipeIsUnknown()
        {
            coffeeMachine.ProcessRequest("TEA");
        }

        [TestMethod]
        public void ShouldGetCappuccinoRecipe()
        {
            var response = coffeeMachine.ProcessRequest("CAPPUCCINO");
            Assert.AreEqual("{ \"espresso\": 1, \"water\": 0, \"milk\": 1, \"foam\": 1 }", response);
        }

        [TestMethod]
        public void ShouldGetMocaccinoRecipe()
        {
            var response = coffeeMachine.ProcessRequest("MOCACCINO");
            Assert.AreEqual("{ \"espresso\": 1, \"water\": 0, \"milk\": 2, \"foam\": 1, \"chocolate\": 1 }", response);
        }
    }
}
